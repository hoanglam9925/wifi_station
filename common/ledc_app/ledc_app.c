#include "ledc_app.h"
#include "stdio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"

void ledcInit(void)
{
    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_13_BIT,
        .freq_hz = 5000,
        .speed_mode = LEDC_HIGH_SPEED_MODE,
        .timer_num = LEDC_TIMER_1,
        .clk_cfg = LEDC_AUTO_CLK,
    };
    ledc_timer_config(&ledc_timer);
}

void ledcAddPin(int pin, int channel)
{
    ledc_channel_config_t ledc_channel = {
        .channel = channel,
        .duty = 0,
        .gpio_num = pin,
        .speed_mode = LEDC_HIGH_SPEED_MODE,
        .hpoint = 0,
        .timer_sel = LEDC_TIMER_1,
    };
    ledc_channel_config(&ledc_channel);
}

void ledcSetDuty(int channel, int duty)
{
    ledc_set_duty(LEDC_HIGH_SPEED_MODE, channel, duty*8191/100);
    ledc_update_duty(LEDC_HIGH_SPEED_MODE, channel);
}
