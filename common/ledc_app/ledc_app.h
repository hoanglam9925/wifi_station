#ifndef _LEDC_APP_H_
#define _LEDC_APP_H_

void ledcInit(void);
void ledcAddPin(int pin, int channel);
void ledcSetDuty(int channel, int duty);

#endif